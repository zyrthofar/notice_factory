module NoticeFactory
  module Builders
    class ApplicationBuilder < NoticeFactory::Builder
      def self.processor_class
        Processors::ApplicationProcessor
      end
    end
  end
end
