module NoticeFactory
  module Builders
    module Appointments
      class AppointmentCreatedBuilder < AppointmentBuilder
        KIND = :appointment_created

        def self.deliverer_class
          Deliverers::Appointments::AppointmentCreatedDeliverer
        end

        protected

        def can_message?
          group.allow_messages? && appointment.service.allow_messages?
        end

        def can_message_about_context?
          appointment.start_time > Time.zone.now
        end
      end
    end
  end
end
