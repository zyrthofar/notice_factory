module NoticeFactory
  module Builders
    module Appointments
      class AppointmentBuilder < ApplicationBuilder
        def appointment
          context
        end

        protected

        def get_group
          appointment.group
        end

        def get_target_messageable
          appointment.patient
        end
      end
    end
  end
end
