module NoticeFactory
  module Processors
    class ApplicationProcessor < NoticeFactory::Processor
      def self.processes(message)
        messageable = message.target_messageable

        [
          {
            contact_kind: :internal,
            contact_value: messageable.id,
            immediate: true,
            wait: {
              duration: 2.minutes,
              condition: ->(delivery) do
                communication = GlobalID::Locator.locate(delivery.reference)
                communication.read?
              end
            },
          }, {
            contact_kind: :text_message,
            contact_value: messageable.text_message_contacts.first,
            requires_confirmation: true
          }, {
            contact_kind: :phone_call,
            contact_value: messageable.phone_number_contacts.first,
            requires_confirmation: true
          }, {
            contact_kind: :email,
            contact_value: messageable.email_contacts.first,
            requires_confirmation: true
          }
        ]
      end
    end
  end
end

# `wait`: A process with a `:wait` attribute will wait for `:duration` before
#   continuing with the next process in line. If the `:condition` returns `true`
#   before the time is expired, though, no more processes will be performed.
#   The typical use-case of this is to let a user some time to do something
#   (eg. read the internal notification) before the system tries to message them
#   with other delivery methods.


# MUST WORK:
#
# [simultaneity] Send a text message and an email at the same time.
