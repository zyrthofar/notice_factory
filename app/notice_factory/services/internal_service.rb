module NoticeFactory
  module Services
    class InternalService
      # `payload` should be of the following form:
      #
      # ```ruby
      # {
      #   source_user_id: 6188,
      #   target_user_id: 6453,
      #   message: 'Message to send to target.'
      # }
      # ```
      def self.deliver(payload)
        # This returns a GlobalID object that will be saved to the database as
        # a string. It can later be converted back to an active record with
        # `GlobalID.parse(gid_string).find`.
        CommunicationService
          .send_communication(
            payload[:source_user_id],
            payload[:target_user_id],
            payload[:message]
          ).to_global_id
      end
    end
  end
end
