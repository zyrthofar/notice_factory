module NoticeFactory
  module Deliverers
    module Appointments
      class AppointmentDeliverer < ApplicationDeliverer
        private

        def appointment
          message.context
        end

        def patient
          message.target_messageable
        end
      end
    end
  end
end
