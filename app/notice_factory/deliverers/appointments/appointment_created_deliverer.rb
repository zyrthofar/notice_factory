module NoticeFactory
  module Deliverers
    module Appointments
      class AppointmentCreatedDeliverer < AppointmentDeliverer
        after_delivery_success :send_push_notifications

        def internal_service_args(delivery)
          super.merge {
            message: "New appointment at #{appointment.start_time}."
          }
        end

        def email_service_args(delivery)
          super.merge {
            subject: 'New Appointment',
            html: "<html>...</html>"
          }
        end

        def text_message_service_args(delivery)
          super.merge {
            body: "New appointment at #{appointment.start_time} for #{patient.display_name}.",
            statusCallback: 'http://...'
          }
        end

        def phone_call_service_args(delivery)
          super.merge {
            url: 'http://...',
            status_callback: 'http://...'
          }
        end

        private

        def self.send_push_notifications(delivery)
          PusherWorker.perform_async(
            "private-patient_group-#{message.group.id}",
            'availability-change',
            [appointment.availability_id]
          )
        end
      end
    end
  end
end
