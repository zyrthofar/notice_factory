module NoticeFactory
  module Deliverers
    class ApplicationDeliverer < NoticeFactory::Deliverer
      def internal_service
        NoticeFactory::Services::InternalService
      end

      def internal_service_args(delivery)
        {
          source_user_id: source.id,
          target_user_id: delivery[:contact_value],
          message: nil
        }
      end

      def email_service
        NoticeFactory::Services::MailgunService
      end

      def email_service_args(delivery)
        {
          from: source.email,
          to: delivery[:contact_value],
          subject: nil,
          html: nil
        }
      end

      def text_message_service
        NoticeFactory::Services::TwilioService::TextMessage
      end

      def text_message_service_args(delivery)
        {
          from: source.phone_numbers.first,
          to: delivery[:contact_value],
          body: nil,
          statusCallback: nil
        }
      end

      def phone_call_service
        NoticeFactory::Services::TwilioService::PhoneCall
      end

      def phone_call_service_args(delivery)
        {
          from: source.phone_numbers.first,
          to: delivery[:contact_value],
          url: delivery[:message_url],
          status_callback: delivery[:status_callback_url]
        }
      end

      private

      def source
        message.source_messageable
      end

      def target
        message.target_messageable
      end
    end
  end
end
