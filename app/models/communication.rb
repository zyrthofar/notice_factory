class Communication < ApplicationRecord
  belongs_to :from_user
  belongs_to :to_user

  attribute :message, :string  
end
