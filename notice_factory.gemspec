$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "notice_factory/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "notice_factory"
  s.version     = NoticeFactory::VERSION
  s.authors     = ["Simon Bernier"]
  s.email       = ["sbernier@petalmd.com"]
  s.homepage    = nil
  s.summary     = "Notice Factory"
  s.description = "Messages and deliveries."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", "~> 5.1.4"

  s.add_development_dependency "sqlite3"
end
