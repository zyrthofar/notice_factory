class CreateContactKinds < ActiveRecord::Migration[5.1]
  def change
    create_table :contact_kinds do |t|
      t.string :code
    end

    NoticeFactory::ContactKind.create!(code: :email)
    NoticeFactory::ContactKind.create!(code: :phone_call)
    NoticeFactory::ContactKind.create!(code: :text_message)
  end
end
