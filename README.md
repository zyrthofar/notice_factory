# NoticeFactory
Short description and motivation.

## Usage
How to use my plugin.

## Installation
Add this line to your application's Gemfile:

```ruby
gem 'notice_factory'
```

And then execute:
```bash
$ bundle
```

Or install it yourself as:
```bash
$ gem install notice_factory
```

## Contributing
Contribution directions go here.

## License
The gem is available as open source under the terms of the [MIT License](http://opensource.org/licenses/MIT).
