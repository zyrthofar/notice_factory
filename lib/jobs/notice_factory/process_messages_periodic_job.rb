module NoticeFactory
  class ProcessMessagesPeriodicJob < ActiveJob::Base
    queue_as :default

    def perform
      message_ids = Message.unprocessed.ids

      # Nothing to send right now.
      return if message_ids.empty?

      message_ids.each do |message_id|
        ProcessMessageJob.perform_later(message_id)
      end
    end
  end
end
