module NoticeFactory
  class ProcessMessageJob < ActiveJob::Base
    queue_as :default

    def perform(message_id)
      message = Message
                  .includes(:context,
                            :source_messageable,
                            :target_messageable,
                            :deliveries)
                  .unprocessed
                  .find_by(id: message_id)

      # The message was not found - this would typically mean that the message
      # was processed in another parallel thread.
      return if message.nil?

      Builder[message.kind].processor_class
        .new(message)
        .process!
    end
  end
end
