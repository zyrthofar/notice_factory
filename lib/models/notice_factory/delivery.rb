module NoticeFactory
  class Delivery < ActiveRecord::Base
    # DATABASE INDICES:
    # * message

    belongs_to :message

    attribute :contact_kind, :string
    attribute :contact_value, :string

    attribute :created_at, :datetime
    attribute :delivered_at, :datetime

    enum status: {
      # The delivery is about to be sent to the service.
      pending: 0,

      # The delivery is being sent to the service.
      delivering: 1,

      # The delivery was sent to the service. No confirmations nor callbacks
      # were received (yet?) but until there are more information, it can be
      # assumed that the delivery went well.
      delivered_unconfirmed: 2,

      # The delivery was confirmed to be successful.
      delivered: 3,

      # Something happened during the delivery. See the `failure_reason`
      # attribute for more information.
      failed: 4
    }

    # TODO: Better name? :result, :target_ref... etc.
    attribute :reference, :string
    attribute :failure_reason, :string

    scope :undelivered { where(status: %i[pending, delivering])}
    scope :succeeded { where(status: delivered_statuses) }

    def succeeded?
      status.in?(delivered_statuses)
    end

    def fail!(reason)
      status = :failed
      failure_reason = reason
      save!
    end

    private

    def delivered_statuses
      %w(delivered_unconfirmed delivered)
    end
  end
end
