module NoticeFactory
  class Delivery < ActiveRecord::Base
    has_many :notifications, through: :notification_deliveries

    belongs_to :contact_kind
    attribute :contact_value, :string

    attribute :at, :datetime

    has_many :statuses, class_name: DeliveryStatus.name

    # attribute :deliver_at, :datetime
    #
    # scope :undelivered, -> { pending.where('deliver_at <= ?', Time.zone.now) }

    def status
      statuses
        .order(at: :desc)
        .limit(1)
        .pluck(:kind)
        .first
        .to_sym
    end

    # Defines the following for all status kinds:
    # * `.kind`
    # * `#kind?`
    # * `#kind!(detail = nil)`
    # * `#kind_status`
    # * `#kind_at`
    DeliveryStatus.kinds.each_key do |kind|
      kind = kind.to_sym

      scope kind, -> do
        # DeliveryCurrentStatus.where(kind: kind)

        kind_id = DeliveryStatus.kinds[kind]
        t2 = DeliveryStatus.select(:delivery_id, 'MAX(at) AS max_at').group(:delivery_id)
        t1 = DeliveryStatus.joins("INNER JOIN (#{t2.to_sql}) t2 ON t2.delivery_id = delivery_statuses.delivery_id AND t2.max_at = at").where(kind: kind_id)
        joins("INNER JOIN (#{t1.to_sql}) t1 ON t1.delivery_id = deliveries.id")
      end

      define_method("#{kind}?") do
        status == kind
      end

      define_method("#{kind}!") do |details = nil|
        statuses
          .create!(kind: kind,
                   at: Time.zone.now,
                   details: details)
      end

      define_method("#{kind}_status") do
        statuses
          .order(at: :desc)
          .find_by(kind: DeliveryStatus.kinds[kind])
      end

      define_method("#{kind}_at") do
        statuses
          .where(kind: DeliveryStatus.kinds[kind])
          .order(at: :desc)
          .limit(1)
          .pluck(:at)
          .first
      end

      # CREATE VIEW delivery_current_status AS
      #   SELECT t1.delivery_id, t1.kind
      #   FROM delivery_statuses t1
      #   INNER JOIN (
      #     SELECT delivery_id, MAX(at) as max_at
      #     FROM delivery_statuses
      #     GROUP BY delivery_id
      #   ) t2 ON t2.delivery_id = t1.delivery_id AND t2.max_at = t1.at
      #   WHERE kind = 0
    end

    scope :succeeded, -> do
      # DeliveryCurrentStatus.where(kind: DeliveryStatus.success_kinds)

      t2 = DeliveryStatus.select(:delivery_id, 'MAX(at) AS max_at').group(:delivery_id)
      t1 = DeliveryStatus.joins("INNER JOIN (#{t2.to_sql}) t2 ON t2.delivery_id = delivery_statuses.delivery_id AND t2.max_at = at").where(kind: DeliveryStatus.success_kind_ids)
      joins("INNER JOIN (#{t1.to_sql}) t1 ON t1.delivery_id = deliveries.id")
    end

    def succeeded?
      status.in?(DeliveryStatus.success_kinds)
    end
  end
end
