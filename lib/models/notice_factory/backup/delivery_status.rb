module NoticeFactory
  class DeliveryStatus < ActiveRecord::Base
    enum kind: {
      # The delivery is waiting to be picked up by a job.
      pending: 0,

      # The delivery is currently being handled by a job.
      processing: 1,

      # The delivery has been sent to the target user. No confirmations were
      # received, but it can be assumed to be successful.
      delivering: 2,

      # The delivery was sent, and a confirmation was received about its
      # success.
      delivered: 3,

      # The delivery has failed. The `failure_reason` attribute has more
      # information about what happened.
      failed: 4
    }

    attribute :at, :datetime
    attribute :details, :string

    def self.success_kinds
      %i[delivering delivered]
    end

    def self.success_kind_ids
      success_kinds.map { |kind| kinds[kind] }
    end
  end
end
