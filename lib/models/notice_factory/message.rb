module NoticeFactory
  class Message < ActiveRecord::Base
    # DATABASE INDICES:
    # * group-context
    # * group-messageable
    # * group-kind
    # * messageable

    # For projects that have data separated in different groups.
    belongs_to :group, polymorphic: true

    # Why the message was sent.
    attribute :kind, :string

    # What the message is about.
    belongs_to :context, polymorphic: true

    # Who initiated the message, or on behalf of whom was the message sent. The
    # group can be used as a source, or even `nil`, meaning "the system".
    belongs_to :source_messageable, polymorphic: true

    # Who is being messaged.
    belongs_to :target_messageable, polymorphic: true

    attribute :created_at, :datetime

    has_many :deliveries

    enum status: {
      # The message is waiting to be picked up by a job.
      pending: 0,

      # The message is currently being processed for delivery.
      processing: 1,

      # The message was processed for delivery. Success or failure is determined
      # by the message's `deliveries`.
      processed: 2
    }

    scope :unprocessed, -> { where.not(status: :processed) }

    def succeeded?
      deliveries.succeeded.exists?
    end

    def failed?
      !succeeded?
    end

    def delivered_at
      deliveries.succeeded.minimum(:delivered_at)
    end
  end
end
