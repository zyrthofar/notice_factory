module NoticeFactory
  class Processor
    attr_reader :message

    def initialize(message)
      @message = message
    end

    def process!
      # The message is already processed - there is nothing to do. This can be
      # caused when multiple jobs are triggered in parallel.
      return false if message.processed?

      # A delivery is still ongoing - stop immediately and do nothing.
      # TODO: Failsafe to prevent deliveries stuck on these statuses (needed?).
      return false if message.deliveries.exists?(status: %i[pending delivering])

      next_process = nil
      deliveries = message.deliveries

      self.class.processes.each do |process|
        delivery = find_delivery(process, deliveries)

        if delivery.nil?
          next_process = process
          break
        end

        next if delivery.failed?

        if process[:wait]
           if process[:wait][:condition].call
            message.processed!
          elsif delivery.delivered_at > Time.zone.now + process[:wait][:duration]
            next
          end
        else
          message.processed!
        end

        return false
      end

      if next_process
        message.processing!

        Builder[message.kind].deliverer_class
          .new(message, next_process)
          .deliver!

        true
      else
        message.processed!
        false
      end
    end

    protected

    def self.processes
      []
    end

    private

    def find_delivery(process, deliveries)
      deliveries.find do |delivery|
        delivery.contact_kind == process[:contact_kind] &&
          delivery.contact_value == process[:contact_value]
      end
    end
  end
end
