module NoticeFactory
  module Services
    class MailgunService
      # `payload` should be of the following form:
      #
      # ```ruby
      # {
      #   from: 'from@example.com',
      #   to: 'to@example.com',
      #   subject: 'subject',
      #   html: '<p>email body</p>'
      # }
      # ```
      #
      # ref. https://documentation.mailgun.com/en/latest/api-sending.html
      # ref. https://github.com/mailgun/mailgun-ruby
      def self.deliver(payload)
        domain = payload.delete(:domain)
        domain ||= payload[:from].split('@').last

        response = Mailgun::Client
                     .new('api_key')
                     .send_message(domain, payload)

        response['id']
      end
    end
  end
end
