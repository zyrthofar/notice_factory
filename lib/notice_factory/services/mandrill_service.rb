module NoticeFactory
  module Services
    class MandrillService
      # `payload` should be of the following form:
      #
      # ```ruby
      # {
      #   from_email: 'from@example.com',
      #   to: [{ email: 'to@example.com', name: 'Mr. To', type: 'to' }],
      #   subject: 'subject',
      #   html: '<p>email body</p>'
      # }
      # ```
      #
      # ref. https://mandrillapp.com/api/docs/messages.JSON.html
      # ref. https://bitbucket.org/mailchimp/mandrill-api-ruby
      def self.deliver(payload)
        response = Mandrill::API
                     .new('api_key')
                     .messages
                     .send(payload)

        response['_id']
      end
    end
  end
end
