module NoticeFactory
  module Services
    module TwilioService
      class PhoneCall
        # `payload` should be of the following form:
        #
        # ```ruby
        # {
        #   from: '(418) 555-1024',
        #   to: '(418) 555-1025',
        #   url: 'https://example.com/phone',
        #   status_callback: 'https://example.com/phone_callback'
        # }
        # ```
        #
        # ref. https://www.twilio.com/docs/api/rest/making-calls
        # ref. https://github.com/twilio/twilio-ruby
        def self.deliver(payload)
          call = Twilio::REST::Client
                   .new('account_sid', 'auth_token')
                   .account
                   .calls
                   .create(payload)

          call.sid
        end
      end

      class TextMessage
        # `payload` should be of the following form:
        #
        # ```ruby
        # {
        #   from: '(418) 555-1024',
        #   to: '(418) 555-1025',
        #   body: 'message',
        #   statusCallback: 'https://example.com/sms_callback'
        # }
        # ```
        #
        # ref. https://www.twilio.com/docs/api/messaging/send-messages
        # ref. https://github.com/twilio/twilio-ruby
        def self.deliver(payload)
          message = Twilio::REST::Client
                      .new('account_sid', 'auth_token')
                      .account
                      .messages
                      .create(payload)

          message.sid
        end
      end
    end
  end
end
