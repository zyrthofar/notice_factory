module NoticeFactory
  class Builder
    attr_reader :group,
                :context,
                :source_messageable,
                :target_messageable,
                :kind

    def initialize(context)
      @context = context
      @group = get_group
      @source_messageable = get_source_messageable
      @target_messageable = get_target_messageable

      # Could the kind be `self.class.name`?
      # Drawback: namespace changes would require data migration.
      @kind = self.class::KIND
    end

    def build!
      return false unless can_message?
      return false unless can_message_about_context?
      return false unless can_message_messageable?
      return false unless can_message_messageable_about_context?

      message = create_message

      # If the first process needs to run immediately, fire the job here. The
      # ProcessMessagesPeriodicJob will catch it later otherwise.
      first_process = self.class.processor_class.processes(message).first
      if first_process&.is_a?(Hash) && first_process[:immediate]
        ProcessMessageJob.perform_later(message.id)
      end

      true
    end

    def self.[](kind)
      Register.retrieve_builder(kind)
    end

    protected

    def self.processor_class
      raise NotImplementedError,
            "Processor for '#{self.class.name}' not implemented."
    end

    def self.deliverer_class
      raise NotImplementedError,
            "Deliverer for '#{self.class.name}' not implemented."
    end

    # The group could be nil, for projects that do not have separations by
    # group.
    def get_group
      nil
    end

    def get_source_messageable
      nil
    end

    # This would actually return an array of messageables.
    def get_target_messageable
      nil
    end

    def can_message?
      true
    end

    def can_message_about_context?
      true
    end

    def can_message_messageable?
      true
    end

    def can_message_messageable_about_context?
      true
    end

    private

    def create_message
      Message.create!(
        group: group,
        kind: kind,
        context: context,
        source_messageable: source_messageable,
        target_messageable: target_messageable,
        status: :pending
      )
    end
  end
end
