module NoticeFactory
  class Deliverer
    include ClassCallbacks

    attr_reader :message,
                :process

    create_callbacks :before_delivery,
                     :after_delivery_success,
                     :after_delivery_failure,
                     on_callback_error: :handle_callback_error

    def initialize(message, process)
      @message = message
      @process = process
    end

    def deliver!
      delivery = create_delivery
      delivery_valid?(delivery) && perform_delivery(delivery)
    end

    protected

    def can_deliver?
      true
    end

    def can_deliver_by_process?
      true
    end

    def can_deliver_to_messageable?
      true
    end

    # Allows last-minute decisions about sending a message, such as phone calls
    # during the night.
    def can_deliver_to_messageable_by_process?
      true
    end

    NoticeFactory.contact_kinds.each do |contact_kind|
      define_method("#{contact_kind}_service") do
        raise NotImplementedError,
              "Service for '#{contact_kind}' not implemented."
      end

      define_method("#{contact_kind}_service_args") do
        raise NotImplementedError,
              "'#{contact_kind}' deliveries not implemented in deliverer."
      end
    end

    private

    def create_delivery
      Delivery.create!(
        message: message,
        contact_kind: process[:contact_kind],
        contact_value: process[:contact_value],
        status: :pending,
        created_at: Time.zone.now
      )
    end

    def delivery_valid?(delivery)
      [
        [
          -> { can_deliver? },
          'can_deliver?'
        ], [
          -> { can_deliver_by_process? },
          'can_deliver_by_process?'
        ], [
          -> { can_deliver_to_messageable? },
          'can_deliver_to_messageable?'
        ], [
          -> { can_deliver_to_messageable_by_process? },
          'can_deliver_to_messageable_by_process?'
        ], [
          # A before_callback returning `false` will cancel the delivery.
          -> { execute_callbacks(:before_delivery, delivery) },
          'before_delivery'
        ]
      ].each do |(lambda, description)|
        if lambda.call.eql?(false)
          return handle_failure(delivery, "`:#{description}` returned `false`.")
        end
      end

      true
    end

    def perform_delivery(delivery)
      service = get_service(delivery.contact_kind)
      args = get_service_args(delivery.contact_kind, delivery)

      delivery.delivering!
      ref = service.deliver(*args)

      delivery.update!(reference: ref)

      handle_success(delivery)
    rescue => error
      handle_failure(delivery, error.message)
    end

    def get_service(contact_kind)
      send("#{contact_kind}_service")
    end

    def get_service_args(contact_kind, delivery)
      send("#{contact_kind}_service_args", delivery)
    end

    def handle_success(delivery)
      if process[:requires_confirmation]
        # Sets the delivery to a status that indicates some other process,
        # typically a service callback, will be expected.
        delivery.delivered_unconfirmed!
      else
        delivery.delivered!
      end

      execute_callbacks(:after_delivery_success, delivery)

      true
    end

    def handle_failure(delivery, reason)
      delivery.fail!("Delivery failed - " + reason)
      execute_callbacks(:after_delivery_failure, delivery)

      false
    end

    def handle_callback_error(error, delivery)
      delivery.fail!(error.message)
    end
  end
end
